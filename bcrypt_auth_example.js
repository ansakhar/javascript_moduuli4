import bcrypt from "bcrypt";
import express from "express";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config();
const hash = process.env.SECRET_HASH;

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.post("/login", async (req, res) => {
    const isCorrectPassword = await bcrypt.compare(req.body.password, hash);
    if(isCorrectPassword) {
        const token = jwt.sign(
            { username: req.body.username },
            process.env.JWT_SECRET,
            { expiresIn: "1h" }
        );

        res.status(200).send({token});
        //res.status(200).send({token: token})
    } else {
        res.status(401).send("Wrong password");
    }
});

const salaisetPolut = express.Router();
app.use("/salaiset", salaisetPolut);

app.use((req, res, next) => {
    const auth = req.get("authorization");
    if (auth && auth.toLowerCase().startsWith("bearer ")) {
        const token = auth.substring(7);
        const decodedToken = jwt.verify(token, process.env.JWT_SECRET);
        if(!token || !decodedToken.username) {
            return res.status(401).json({error: "token missing or invalid"});
        }
        req.user = decodedToken;
        next();
    } else {
        return res.status(401).json({error: "token missing or invalid"});
    }
});

salaisetPolut.get("/polku", (req, res) => {
    res.send("salainen polku käyttäjälle " + req.user.username);
});

salaisetPolut.get("/userdata", (req, res) => {
    console.log("userdata");
    res.send("Secret userdata for user " + req.user.username);
});


app.listen(5000, () => {
    console.log("Listening to port 5000");
});