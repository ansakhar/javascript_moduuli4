import express from "express";
import { v4 as uuidv4 } from "uuid";
import fs from "fs";

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

let students = [];

function updateJsonFile() {
    fs.writeFileSync("db.json", JSON.stringify(students), "utf-8");
}

function readJsonFile() {
    try {
        students = JSON.parse(fs.readFileSync("db.json", "utf-8"));
    } catch {
        students = [];
    }
}

app.get("/students", (req, res) => {
    res.send(students);
});

app.get("/students/:id", (req, res) => {
    const student = students.find(student => student.id === req.params.id);
    res.send(student);
});

app.post("/students", (req, res) => {
    const student = req.body;
    student.id = uuidv4();
    console.log(students);
    students = students.concat(req.body);
    updateJsonFile();
    res.redirect("/");
});


app.put("/students/:id", (req, res) => {
    const newStudent = {...req.body, id: req.params.id};
    /*
    const newStudent = req.body;
    newStudent.id = req.params.id;
    */
    students = students.map(student => 
        student.id === req.params.id ? newStudent : student
    );
    /*students = students.map(student => {
        if(student.id === req.params.id) {
            return req.body;
        } else {
            return student;
        }
    });*/
    updateJsonFile();
    res.redirect("/");
});


app.delete("/students/:id", (req, res) => {
    students = students.filter(student => student.id !== req.params.id);
    updateJsonFile();
    res.redirect("/");
});

readJsonFile();
app.listen(5000, () => {
    console.log("Listening to port 5000");
});