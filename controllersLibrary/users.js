import express from "express";
import fs from "fs";
import bcrypt, { compare } from "bcrypt";
import { v4 as uuidv4 } from "uuid";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import authRouter from "./auth.js";

dotenv.config(); // access environment variables
const jwtKey = process.env.APP_SECRET;

const usersRouter = express.Router();

const saltRounds = 10;

let users = []; // user: {userId: random ID, username, passwordHash, frends[frendsname: , frendsID]}

function updateUsersJsonFile() {
    fs.writeFileSync("library_users.json", JSON.stringify(users), "utf-8");
}

export function readUsersJsonFile() {
    try {
        users = JSON.parse(fs.readFileSync("library_users.json", "utf-8"));
    } catch {
        users = [];
    }
}

async function getUser(userProperty) {
    try {
        //const findedUser = users.filter(user => user.username === username);
        const findedUser = users.find(user => 
            ((user.username === userProperty)||(user.userId === userProperty)));
        //console.log("findedUser", findedUser);
        return findedUser;
    } catch (err) {
        console.log(err);
    }
}

usersRouter.get("/users", (req, res) => {
    res.send(users);
    //res.json(users)
});

usersRouter.post("/register", async(req, res) => { // try ... catch ??
    const user = {};
    user.userId = uuidv4();
    user.username = req.body.username;
    user.passwordHash = await bcrypt.hash(req.body.password, saltRounds);
    user.friends = [];
    users = users.concat(user);
    res.json(user);
    updateUsersJsonFile();
});

usersRouter.post("/login", async(req, res) => {
    const body = req.body;
    const user = await getUser(body.username);
    //console.log("user", user);
    const passwordCorrect = user === undefined
        ? false
        : await compare(body.password, user.passwordHash);

    if (!(user && passwordCorrect))  {
        //res.status(401).send("Invalid uername or password");
        return res.status(401).json({
            error: "Invalid username or password"
        });
    }
    else {
        // a new JWT token will be created if username and password are correct:
        const token = jwt.sign({ userId: user.userId},
            jwtKey,
            { expiresIn: 60*60 }
        );
        res.status(200).send({token, userId: user.userId});
    }
});

usersRouter.use("/auth", authRouter);

/*
POST /friends → Add a username into the “friends” array of the user that sent the request.
The username has to belong to a user in the “users” list
*/

authRouter.post("/friends", async(req, res) => {
    const friend = req.body;
    const user = await getUser(req.user.userId);
    const foundFriend = await getUser(friend.username);
    if(foundFriend) {
        user.friends = user.friends.concat({username: friend.username, userId: foundFriend.userId});
        res.json(user);
        updateUsersJsonFile();
    }
    else {
        res.status(404).send({ error: `user '${friend.username}' not found`});
    }
});

/*
GET /friends → As a response, return a list that has usernames from the “friends” array of
the user that sent the request.
*/
authRouter.get("/friends", async(req, res) => {
    const user = await getUser(req.user.userId);
    res.send(user.friends);
});

/*
GET /friendbooks → As a response, return a list of all the books that your friends are
owners of.
*/ 
// ei vielä tehty:
authRouter.get("/friendsbooks", async(req, res) => {
    const user = await getUser(req.user.userId);
    res.send(user.friends);
});

export default usersRouter;