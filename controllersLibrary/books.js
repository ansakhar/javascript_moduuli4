/*
Only the owner should be able to modify or get their own books.
*/
import express from "express";
import fs from "fs";
import authRouter from "./auth.js";

let books = [];
/* book: {
    id: number,
    name: string,
    author: string,
    read: boolean,
    userId
    }
*/

function updateJsonFile() {
    fs.writeFileSync("library.json", JSON.stringify(books), "utf-8");
}

export function readJsonFile() {
    try {
        books = JSON.parse(fs.readFileSync("library.json", "utf-8"));
    } catch {
        books = [];
    }
}

function nextId() {
    const id = books.reduce( (acc, val) => {
        if(val.id >= acc) {
            return val.id + 1;
        } else {
            return acc;
        }
    }, 1);
    return id;
}

const booksRouter = express.Router();

booksRouter.get("/", (req, res) => {
    res.send(books);
    //res.json(books)
});

booksRouter.post("/", (req, res) => {
    const book = req.body;
    book.id = nextId();
    books = books.concat(req.body);
    res.json(book);
    updateJsonFile();
    //res.redirect("/");
});

booksRouter.use("/auth", authRouter);

authRouter.get("/books/:id", (req, res) => {
    const bookId = parseInt(req.params.id);
    const book = books.find(book => book.id === bookId);
    console.log("finded book: ", book);
    if (book) {
        if(req.user.userId === book.userId) { // check if a book is in the user's library
            res.send(book);
        }
        else {
            res.status(405).send({ error: `book with id '${bookId}' is not in your library`});
        }
    }
    else res.status(404).end();
    //res.send(book);
});

authRouter.put("/books/:id", (req, res) => {
    const bookId = parseInt(req.params.id);
    const body = req.body;
    //console.log("req.user", req.user);
    const book = books.find(book => book.id === bookId);
    if(book) {
        if(req.user.userId === book.userId) { // check if a book is in the user's library
            const newbook = {name: body.name, author: body.author, read: body.read, userId: book.userId, id: bookId};
            books = books.map(book => 
                book.id === bookId ? newbook : book
            );
            updateJsonFile();
            //res.redirect("/");
            res.status(200).send(newbook);
        }
        else {
            res.status(405).send({ error: `book with id '${bookId}' is not in your library`});
        }
    } else {
        res.status(404).send({ error: `book with id '${bookId}' not found`});
    }
});

authRouter.delete("/books/:id", (req, res) => {
    const bookId = parseInt(req.params.id);
    const book = books.find(book => book.id === bookId);
    if(book) {
        if(req.user.userId === book.userId) { // check if a book is in the user's library
            books = books.filter(book => book.id !== bookId);
            updateJsonFile();
            res.send(`book with id '${bookId}' deleted `);
        }
        else {
            res.status(405).send({ error: `book with id '${bookId}' is not in your library`});
        }
    }
    else res.status(404).send({ error: `book with id '${bookId}' not found`});
    //res.redirect("/");
});

export default booksRouter;

//
