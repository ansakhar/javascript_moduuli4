import express from "express";
import { compare } from "bcrypt";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";

dotenv.config(); // access environment variables

const app = express();
app.use(express.json());
//app.use(express.urlencoded({extended: false}));

const users = [{username:"Anna", passwordHash:"$2b$10$1fRRA7JtPXIdr9zX7VyvaOidNcKYs7e5KkKzzFIdr91knDwfGaHae"}];
console.log(users);

const jwtKey = process.env.APP_SECRET;

async function getUser(username) {
    try {
        const findedUser = users.filter(user => user.username === username);
        console.log("findedUser", findedUser[0]);
        return findedUser[0];
    } catch (err) {
        console.log(err);
    }
}

app.post("/login", async(req, res) => {
    const body = req.body;
    const user = await getUser(body.username);
    console.log("user", user);
    const passwordCorrect = user === null
        ? false
        : await compare(body.password, user.passwordHash);

    if (!(user && passwordCorrect))  {
        //res.status(401).send("Invalid uername or password");
        return res.status(401).json({
            error: "Invalid uername or password"
        });
    }
    else {
        // a new JWT token will be created if username and password are correct:
        const token = jwt.sign({ username: req.body.username },
            jwtKey,
            { expiresIn: 60*60 }
        );
        res.status(200).send({token, username: user.username});
    }
});

app.get("/getSecrets", (request, response) => {
    // get authorization token
    const authorization = request.get("authorization");
    // remove bearer word from the token
    let token;
    if (authorization && authorization.toLowerCase().startsWith("bearer ")) {
        token = authorization.substring(7);    }
  
    // check that token in is valid with the user
    let payload;
    try {
        payload = jwt.verify(token, jwtKey);
    } catch(error) {
        return response.status(401).send( { error: "JWT token is unauthorized!" } );
    }
  
    // token is valid send GET sample response back to the client
    response.send( { info: `GET request with ${payload.username} successfully received!`} );
  
});

app.listen(5000, () => {
    console.log("Listening to port 5000");
});
