import fs from "fs";
import { v4 as uuidv4 } from "uuid";

let students = [];

function updateJsonFile() {
    fs.writeFileSync("db.json", JSON.stringify(students), "utf-8");
}

export function readJsonFile() {
    try {
        students = JSON.parse(fs.readFileSync("db.json", "utf-8"));
    } catch {
        students = [];
    }
}

export const getAllStudents = (req, res) => {
    res.send(students);
};

export const getStudent = (req, res) => {
    const student = students.find(student => student.id === req.params.id);
    res.send(student);
};

export const postStudent = (req, res) => {
    const student = req.body;
    student.id = uuidv4();
    students = students.concat(req.body);
    updateJsonFile();
    res.redirect("/");
};

export const putStudent = (req, res) => {
    const newStudent = {...req.body, id: req.params.id};
    students = students.map(student => 
        student.id === req.params.id ? newStudent : student
    );
    updateJsonFile();
    res.redirect("/");
};

export const deleteStudent = (req, res) => {
    students = students.filter(student => student.id !== req.params.id);
    updateJsonFile();
    res.redirect("/");
};
