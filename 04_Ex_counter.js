import express from "express";
const app = express();
let count = 0;
//let count2;
app.get("/counter", (req, res) => {
    if(req.query.number)
        count = req.query.number;
    else count++;
    //count = count2 + 1;
    console.log("Params:");
    console.log(req.params);
    console.log("Query:");
    console.log(req.query);
    res.send("<h1> Counter: " + count + "</h1>");
});

const counterName = {};
//counterName["Peetu"] = 0;
//counterName["Rene"] = 0;
app.get("/counter/:name", (req, res) => {
    if(!counterName[req.params.name]) counterName[req.params.name] = 0;
    counterName[req.params.name]++;
        
    console.log("Params:");
    console.log(req.params);
    console.log("Query:");
    console.log(req.query);
    res.send("<h1> Peetu was here " + counterName[req.params.name] + " times. </h1>");
});

app.listen(5000, () => {
    console.log("Listening to port 5000");
}); // Then connect to localhost:5000 with your browse