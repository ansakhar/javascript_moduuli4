//import http from "http";
import express from "express";
const app = express();

/*const server = http.createServer((req, res) => {
 res.write("Some cool response!");
 res.end();
});*/

app.get("/", (req, res) => {
    res.send("Hello world!");
});

app.get("/endpoint2", (req, res) => {
    res.send("Maanantai...");
});

app.get("/:name/:surname", (req, res) => {
    console.log("Params:");
    console.log(req.params);
    console.log("Query:");
    console.log(req.query);
    res.send("Hello " + req.params.name);
});

//server.listen(5000);
app.listen(5000, () => {
    console.log("Listening to port 5000");
}); // Then connect to localhost:5000 with your browse