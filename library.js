import express from "express";
//import fs from "fs";
import path from "path";
import booksRouter, { readJsonFile } from "./controllersLibrary/books.js";
import usersRouter, { readUsersJsonFile } from "./controllersLibrary/users.js";

const __dirname = path.resolve();
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("publicLibrary"));

app.use("/books", booksRouter);
app.use("/", usersRouter);

// initial page: ui form
app.get("/library", (request, response) => {
    response.sendFile(path.join(__dirname + "/publicLibrary/library.html"));
});

readJsonFile();
readUsersJsonFile();

app.listen(5000, () => {
    console.log("Listening to port 5000");
});