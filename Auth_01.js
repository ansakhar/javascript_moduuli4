import bcrypt from "bcrypt";
import * as readline from "node:readline";

const rl = readline.createInterface({
    input: process.stdin, 
    output: process.stdout
});

const saltRounds = 10;
const password = "pass";
const correctPassword = await bcrypt.hash(password, saltRounds);

/*const comparePassword = async (password, hash) => {
    try {
        // Compare password
        return await bcrypt.compare(password, hash);
    } catch (error) {
        console.log("error");
    }
    // Return false if error
    return false;
};*/
function main() {
    rl.question("password?: ", async(input) => {
        const isValidPass = await bcrypt.compare(input, correctPassword);
        if (isValidPass) console.log("Logged in");
        else console.log("Wrong password");
        process.exit(1);
    });
}


main();