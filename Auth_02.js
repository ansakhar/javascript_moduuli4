import express from "express";
import bcrypt, { compare } from "bcrypt";
import dotenv from "dotenv";

dotenv.config();

const app = express();
app.use(express.json());
//app.use(express.urlencoded({extended: false}));

//const saltRounds = 10;
const correctPassword = process.env.CORRECT_PASSWORD;
//const correctPassword = await bcrypt.hash(password, saltRounds);
console.log(correctPassword);
const correctUsername = process.env.CORRECT_USERNAME;

app.post("/login", async(req, res) => {
    const user = req.body;
    console.log(user);
    const isValidPass = await compare(user.password, correctPassword);
    if ((isValidPass)&&(user.username === correctUsername))  {
    //console.log("Logged in");
        res.status(200).send("Logged in");
    }
    else {
        // console.log("Wrong password");
        res.status(401).send("Wrong password");
    }
   
});

/*app.post("/login", (req, res) => {
    bcrypt.compare(req.body.password, correctPassword, (err, isCorrectPassword) => {
        if(isCorrectPassword) {
            res.status(200).send("Access granted!");
        } else {
            res.status(401).send("Wrong password");
        }
    });
}); */

/*...bcrypt.compare(req.body.password, hash)
    .then((isCorrectPassword) => {
        if(isCorrectPassword) {
            res.status(200).send("Access granted!");
        } else {
            res.status(401).send("Wrong password");
        }
    })
    .catch(err => console.log(err));...*/


app.listen(7000, () => {
    console.log("Listening to port 7000");
});
