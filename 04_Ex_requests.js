import express from "express";
const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

const students = [];

app.get("/", () => {
    console.log("GET request init!");
});

app.get("/students", (req, res) => {
    console.log("GET /students request init!");
    console.log("All students:", students);
    res.send(students);
});

app.get("/student/:id", (req, res) => {
    console.log("GET /student/:id request init!");
    const findId = parseInt(req.params.id);
    const student = students.find(element => element.id === findId);
    console.log(student);
    res.json(student);
});
  
app.post("/student/", (request, response) => {
    console.log("POST request init!");
    console.log(request.body);
    const student = {
        name: request.body.name,
        id: request.body.id,
        email: request.body.email
    };
    students.push(student);
    //students.push(request.body);
    response.redirect("/");
});

app.put("/student/:id", (request, response) => {
    console.log("PUT request init!");
    console.log(request.body);
    const findId = parseInt(request.params.id);
    const student = {
        name: request.body.name,
        id: request.body.id,
        email: request.body.email
    };
    const index = students.findIndex(element => element.id === findId);
    students[index] = student;
    //students[index] = request.body;
    response.redirect("/");
});

app.delete("/student/:id", (req, res) => {
    console.log("DELETE /student/:id request init!");
    const findId = parseInt(req.params.id);
    const index = students.findIndex(element => element.id === findId);
    students.splice(index,1);
    res.redirect("/");
});

app.listen(5000, () => {
    console.log("Listening to port 5000");
}); // Then connect to localhost:5000 with your browse