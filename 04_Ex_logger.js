import express from "express";
const app = express();
const port = 5000;
import * as fs from "fs";
/*
fs.appendFile("log.txt", "data to append", function (err) {
  if (err) throw err;
  console.log("Saved!");
});*/

app.use((request, response, next) => {
    console.log("hello from middleware");
    console.log("body:", request.body);
    console.log("Query variables:", request.query);
    console.log("Query parameters:", request.params);
    const log = ` The link of the request: http://localhost:${port}${request.originalUrl}`;
    console.log(log);
    fs.appendFile("log.txt", new Date().toISOString() + log + "\n" + JSON.stringify(request.query) + "\n", function (err) {
        if (err) console.log(err);
    });
    /*request.body; // JavaScript object containing the parse JSON
  response.json(request.body);*/
    next();
});


let count = 0;
app.get("/counter", (req, res) => {
    if(req.query.number)
        count = req.query.number;
    else count++;
    res.send("<h1> Counter: " + count + "</h1>");
});

const counterName = {};
//counterName["Peetu"] = 0;
//counterName["Rene"] = 0;
app.get("/counter/:name", (req, res) => {
    if(!counterName[req.params.name]) counterName[req.params.name] = 0;
    counterName[req.params.name]++;
        
    /*console.log("Params:");
    console.log(req.params);
    console.log("Query:");
    console.log(req.query);*/
    res.send("<h1> Peetu was here " + counterName[req.params.name] + " times. </h1>");
});

app.listen(port, () => {
    console.log("Listening to port 5000");
}); // Then connect to localhost:5000 with your browse
